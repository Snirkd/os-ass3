#include "param.h"
#include "types.h"
#include "defs.h"
#include "x86.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"
#include "elf.h"

extern char data[];  // defined by kernel.ld
pde_t *kpgdir;  // for use in scheduler()

// Forward decs // AVI 18/5
void swap_pages(struct proc* curproc);
void pages_reorder(int chosen);
int chose_nfua(struct proc* p);
int chose_lapa(struct proc* p);
int chose_scfifo(struct proc* p);
int chose_aq(struct proc* p);



// Set up CPU's kernel segment descriptors.
// Run once on entry on each CPU.
void
seginit(void)
{
  struct cpu *c;

  // Map "logical" addresses to virtual addresses using identity map.
  // Cannot share a CODE descriptor for both kernel and user
  // because it would have to have DPL_USR, but the CPU forbids
  // an interrupt from CPL=0 to DPL=3.
  c = &cpus[cpuid()];
  c->gdt[SEG_KCODE] = SEG(STA_X|STA_R, 0, 0xffffffff, 0);
  c->gdt[SEG_KDATA] = SEG(STA_W, 0, 0xffffffff, 0);
  c->gdt[SEG_UCODE] = SEG(STA_X|STA_R, 0, 0xffffffff, DPL_USER);
  c->gdt[SEG_UDATA] = SEG(STA_W, 0, 0xffffffff, DPL_USER);
  lgdt(c->gdt, sizeof(c->gdt));
}

// Return the address of the PTE in page table pgdir
// that corresponds to virtual address va.  If alloc!=0,
// create any required page table pages.
static pte_t *
walkpgdir(pde_t *pgdir, const void *va, int alloc)
{
  pde_t *pde;
  pte_t *pgtab;

  pde = &pgdir[PDX(va)];
  if(*pde & PTE_P){
    pgtab = (pte_t*)P2V(PTE_ADDR(*pde));
  } else {
    if(!alloc || (pgtab = (pte_t*)kalloc()) == 0)
      return 0;
    // Make sure all those PTE_P bits are zero.
    memset(pgtab, 0, PGSIZE);
    // The permissions here are overly generous, but they can
    // be further restricted by the permissions in the page table
    // entries, if necessary.
    *pde = V2P(pgtab) | PTE_P | PTE_W | PTE_U;
  }
  return &pgtab[PTX(va)];
}

// Create PTEs for virtual addresses starting at va that refer to
// physical addresses starting at pa. va and size might not
// be page-aligned.
static int
mappages(pde_t *pgdir, void *va, uint size, uint pa, int perm)
{
  char *a, *last;
  pte_t *pte;

  a = (char*)PGROUNDDOWN((uint)va);
  last = (char*)PGROUNDDOWN(((uint)va) + size - 1);
  for(;;){
    if((pte = walkpgdir(pgdir, a, 1)) == 0)
      return -1;
    if(*pte & PTE_P)
      panic("remap");
    *pte = pa | perm | PTE_P;
    if(a == last)
      break;
    a += PGSIZE;
    pa += PGSIZE;
  }
  return 0;
}

// There is one page table per process, plus one that's used when
// a CPU is not running any process (kpgdir). The kernel uses the
// current process's page table during system calls and interrupts;
// page protection bits prevent user code from using the kernel's
// mappings.
//
// setupkvm() and exec() set up every page table like this:
//
//   0..KERNBASE: user memory (text+data+stack+heap), mapped to
//                phys memory allocated by the kernel
//   KERNBASE..KERNBASE+EXTMEM: mapped to 0..EXTMEM (for I/O space)
//   KERNBASE+EXTMEM..data: mapped to EXTMEM..V2P(data)
//                for the kernel's instructions and r/o data
//   data..KERNBASE+PHYSTOP: mapped to V2P(data)..PHYSTOP,
//                                  rw data + free physical memory
//   0xfe000000..0: mapped direct (devices such as ioapic)
//
// The kernel allocates physical memory for its heap and for user memory
// between V2P(end) and the end of physical memory (PHYSTOP)
// (directly addressable from end..P2V(PHYSTOP)).

// This table defines the kernel's mappings, which are present in
// every process's page table.
static struct kmap {
  void *virt;
  uint phys_start;
  uint phys_end;
  int perm;
} kmap[] = {
 { (void*)KERNBASE, 0,             EXTMEM,    PTE_W}, // I/O space
 { (void*)KERNLINK, V2P(KERNLINK), V2P(data), 0},     // kern text+rodata
 { (void*)data,     V2P(data),     PHYSTOP,   PTE_W}, // kern data+memory
 { (void*)DEVSPACE, DEVSPACE,      0,         PTE_W}, // more devices
};

// Set up kernel part of a page table.
pde_t*
setupkvm(void)
{
  pde_t *pgdir;
  struct kmap *k;

  if((pgdir = (pde_t*)kalloc()) == 0)
    return 0;
  memset(pgdir, 0, PGSIZE);
  if (P2V(PHYSTOP) > (void*)DEVSPACE)
    panic("PHYSTOP too high");
  for(k = kmap; k < &kmap[NELEM(kmap)]; k++)
    if(mappages(pgdir, k->virt, k->phys_end - k->phys_start,
                (uint)k->phys_start, k->perm) < 0) {
      freevm(pgdir);
      return 0;
    }
  return pgdir;
}

// Allocate one page table for the machine for the kernel address
// space for scheduler processes.
void
kvmalloc(void)
{
  kpgdir = setupkvm();
  switchkvm();
}

// Switch h/w page table register to the kernel-only page table,
// for when no process is running.
void
switchkvm(void)
{
  lcr3(V2P(kpgdir));   // switch to the kernel page table
}

// Switch TSS and h/w page table to correspond to process p.
void
switchuvm(struct proc *p)
{
  if(p == 0)
    panic("switchuvm: no process");
  if(p->kstack == 0)
    panic("switchuvm: no kstack");
  if(p->pgdir == 0)
    panic("switchuvm: no pgdir");

  pushcli();
  mycpu()->gdt[SEG_TSS] = SEG16(STS_T32A, &mycpu()->ts,
                                sizeof(mycpu()->ts)-1, 0);
  mycpu()->gdt[SEG_TSS].s = 0;
  mycpu()->ts.ss0 = SEG_KDATA << 3;
  mycpu()->ts.esp0 = (uint)p->kstack + KSTACKSIZE;
  // setting IOPL=0 in eflags *and* iomb beyond the tss segment limit
  // forbids I/O instructions (e.g., inb and outb) from user space
  mycpu()->ts.iomb = (ushort) 0xFFFF;
  ltr(SEG_TSS << 3);
  lcr3(V2P(p->pgdir));  // switch to process's address space
  popcli();
}

// Load the initcode into address 0 of pgdir.
// sz must be less than a page.
void
inituvm(pde_t *pgdir, char *init, uint sz)
{
  char *mem;

  if(sz >= PGSIZE)
    panic("inituvm: more than a page");
  mem = kalloc();
  memset(mem, 0, PGSIZE);
  mappages(pgdir, 0, PGSIZE, V2P(mem), PTE_W|PTE_U);
  memmove(mem, init, sz);
}

// Load a program segment into pgdir.  addr must be page-aligned
// and the pages from addr to addr+sz must already be mapped.
int
loaduvm(pde_t *pgdir, char *addr, struct inode *ip, uint offset, uint sz)
{
  uint i, pa, n;
  pte_t *pte;

  if((uint) addr % PGSIZE != 0)
    panic("loaduvm: addr must be page aligned");
  for(i = 0; i < sz; i += PGSIZE){
    if((pte = walkpgdir(pgdir, addr+i, 0)) == 0)
      panic("loaduvm: address should exist");
    pa = PTE_ADDR(*pte);
    if(sz - i < PGSIZE)
      n = sz - i;
    else
      n = PGSIZE;
    if(readi(ip, P2V(pa), offset+i, n) != n)
      return -1;
  }
  return 0;
}

// Allocate page tables and physical memory to grow process from oldsz to
// newsz, which need not be page aligned.  Returns new size or 0 on error.
int
allocuvm(pde_t *pgdir, uint oldsz, uint newsz)
{
  char *mem;
  uint a;

  if(newsz >= KERNBASE)
    return 0;
  if(newsz < oldsz)
    return oldsz;

  a = PGROUNDUP(oldsz);
  for(; a < newsz; a += PGSIZE){
    
    #if defined (NFUA) || defined (LAPA) || defined (SCFIFO) || defined (AQ)
      // Check if there is room in physical mem, if not, try to swap pages.
      if((myproc()) && strncmp(myproc()->name, "init", 4) != 0 && strncmp(myproc()->name, "sh", 4) != 0){
        if(myproc()->pageTable.phys_pagecount == MAX_PSYC_PAGES){
          if(myproc()->pageTable.swp_pagecount == MAX_TOTAL_PAGES){
            // Allocation exceeded 32 pages, deallocate and return 0
            deallocuvm(pgdir, newsz, oldsz);
            return 0;
          }else{
            // Swap pages from physical to swap with the policies CONTINUE HERE 15/4
            swap_pages(myproc());
          }
        }
      }
    
    #endif 

    mem = kalloc();
    if(mem == 0){
      cprintf("allocuvm out of memory\n");
      deallocuvm(pgdir, newsz, oldsz);
      return 0;
    }
    memset(mem, 0, PGSIZE);
    if(mappages(pgdir, (char*)a, PGSIZE, V2P(mem), PTE_W|PTE_U) < 0){
      cprintf("allocuvm out of memory (2)\n");
      deallocuvm(pgdir, newsz, oldsz);
      kfree(mem);
      return 0;
    }
    // AVI 18/5
    #if defined (NFUA) || defined (LAPA) || defined (SCFIFO) || defined (AQ)
      if((myproc()) && strncmp(myproc()->name, "init", 4) != 0 && strncmp(myproc()->name, "sh", 4) != 0){
        uint* pg_base = (uint*)PGROUNDDOWN(a);
        pte_t* pte = walkpgdir(pgdir,pg_base,0);        
        add_phys_page(PGROUNDDOWN(a), pte);
      }
    #endif
  }
  return newsz;
}

// Deallocate user pages to bring the process size from oldsz to
// newsz.  oldsz and newsz need not be page-aligned, nor does newsz
// need to be less than oldsz.  oldsz can be larger than the actual
// process size.  Returns the new process size.
int
deallocuvm(pde_t *pgdir, uint oldsz, uint newsz)
{
  pte_t *pte;
  uint a, pa;

  if(newsz >= oldsz)
    return oldsz;

  a = PGROUNDUP(newsz);
  for(; a  < oldsz; a += PGSIZE){
    pte = walkpgdir(pgdir, (char*)a, 0);
    if(!pte)
      a = PGADDR(PDX(a) + 1, 0, 0) - PGSIZE;
    else if((*pte & PTE_P) != 0){
      
      #ifndef NONE // AVI 18/5
        if (myproc()->pgdir == pgdir && strncmp(myproc()->name, "init", 4) != 0 && strncmp(myproc()->name, "sh", 4) != 0) {
          int index = -1;
          int i;
          for(i = 0 ; i < MAX_PSYC_PAGES ; i++) {
            if (myproc()->pageTable.phys_pgtable[i].in_physmem && a == myproc()->pageTable.phys_pgtable[i].virt_addr){
              index = i;
              break;
            }
          }

          if(index == -1)
            panic("Page not in physical memory");

          reset_page_data(&myproc()->pageTable.phys_pgtable[index]);
          pages_reorder(index);
          myproc()->pageTable.phys_pagecount--;
        }
      #endif
      
      pa = PTE_ADDR(*pte);
      if(pa == 0)
        panic("kfree");
      char *v = P2V(pa);
      kfree(v);
      *pte = 0;
    }

    #ifndef NONE
    else if (pgdir == myproc()->pgdir && (*pte & PTE_PG) != 0 ) { // if the page was paged out to secondary storage 
      // find the page and remove it
      if(myproc() && strncmp(myproc()->name, "init", 4) != 0 && strncmp(myproc()->name, "sh", 4) != 0){
        int i;
        for(i = 0; i < MAX_SWAP_PAGES; i++) {
          if (a == myproc()->pageTable.swp_pgtable[i].virt_addr){
            reset_page_data(&myproc()->pageTable.swp_pgtable[i]);
            myproc()->pageTable.swp_pagecount--;
          }
        }
      }
    }
      // No need to remove from file, because the next time we will write on it.
    #endif
  }
  return newsz;
}

// Free a page table and all the physical memory pages
// in the user part.
void
freevm(pde_t *pgdir)
{
  uint i;

  if(pgdir == 0)
    panic("freevm: no pgdir");
  deallocuvm(pgdir, KERNBASE, 0);
  for(i = 0; i < NPDENTRIES; i++){
    if(pgdir[i] & PTE_P){
      char * v = P2V(PTE_ADDR(pgdir[i]));
      kfree(v);
    }
  }
  kfree((char*)pgdir);
}

// Clear PTE_U on a page. Used to create an inaccessible
// page beneath the user stack.
void
clearpteu(pde_t *pgdir, char *uva)
{
  pte_t *pte;

  pte = walkpgdir(pgdir, uva, 0);
  if(pte == 0)
    panic("clearpteu");
  *pte &= ~PTE_U;
}

// Given a parent process's page table, create a copy
// of it for a child.
pde_t*
copyuvm(pde_t *pgdir, uint sz)
{
  pde_t *d;
  pte_t *pte;
  uint pa, i, flags;
  char *mem;

  if((d = setupkvm()) == 0)
    return 0;
  for(i = 0; i < sz; i += PGSIZE){
    if((pte = walkpgdir(pgdir, (void *) i, 0)) == 0)
      panic("copyuvm: pte should exist");
    // if(!(*pte & PTE_P))
    //   panic("copyuvm: page not present");
    if(!(*pte & PTE_P)){ // AVI 18/5
      if(!(*pte & PTE_PG))
        // Page is not on physical memory and not in the swap file
        panic("copyuvm: page not present");
      continue;
    }
    pa = PTE_ADDR(*pte);
    flags = PTE_FLAGS(*pte);
    if((mem = kalloc()) == 0)
      goto bad;
    memmove(mem, (char*)P2V(pa), PGSIZE);
    if(mappages(d, (void*)i, PGSIZE, V2P(mem), flags) < 0)
      goto bad;
  }
  return d;

bad:
  freevm(d);
  return 0;
}

//PAGEBREAK!
// Map user virtual address to kernel address.
char*
uva2ka(pde_t *pgdir, char *uva)
{
  pte_t *pte;

  pte = walkpgdir(pgdir, uva, 0);
  if((*pte & PTE_P) == 0)
    return 0;
  if((*pte & PTE_U) == 0)
    return 0;
  return (char*)P2V(PTE_ADDR(*pte));
}

// Copy len bytes from p to user address va in page table pgdir.
// Most useful when pgdir is not the current page table.
// uva2ka ensures this only works for PTE_U pages.
int
copyout(pde_t *pgdir, uint va, void *p, uint len)
{
  char *buf, *pa0;
  uint n, va0;

  buf = (char*)p;
  while(len > 0){
    va0 = (uint)PGROUNDDOWN(va);
    pa0 = uva2ka(pgdir, (char*)va0);
    if(pa0 == 0)
      return -1;
    n = PGSIZE - (va - va0);
    if(n > len)
      n = len;
    memmove(pa0 + (va - va0), buf, n);
    len -= n;
    buf += n;
    va = va0 + PGSIZE;
  }
  return 0;
}

//PAGEBREAK!
// Blank page.
//PAGEBREAK!
// Blank page.
//PAGEBREAK!
// Blank page.

// Copy a page's meta data from page2 to page1
void
copy_page_data(struct page_data* page1, struct page_data* page2){
	page1->virt_addr = page2->virt_addr;
	page1->page_tbl_entry = page2->page_tbl_entry;
	page1->in_swp = page2->in_swp;
	page1->in_physmem = page2->in_physmem;
	page1->file_offset = page2->file_offset;
  page1->access_count = page2->access_count;
  page1->creation_time = page2->creation_time;
}

void
reset_page_data(struct page_data* page){
	page->virt_addr = 0;
	page->page_tbl_entry = 0;
	page->in_swp = 0;
	page->in_physmem = 0;
	page->file_offset = -1;
  #if defined (NFUA) || defined (SCFIFO) || defined (AQ)
  page->access_count = 0;
  #endif

  #ifdef LAPA
  page->access_count = 0xFFFFFFFF;
  #endif
  
  page->creation_time = ticks;
}

// SNIR 14/5
void
reset_pages(struct proc* proc){
  if((myproc()) && strncmp(myproc()->name, "init", 4) != 0 && strncmp(myproc()->name, "sh", 4) != 0){
    proc->pageTable.phys_pagecount = 0;
    proc->pageTable.swp_pagecount = 0;
    proc->pageTable.pgflt_count = 0;
    proc->pageTable.pgout_count = 0;

    int i;
    for(i = 0; i < MAX_PSYC_PAGES; i++){
      reset_page_data(&proc->pageTable.phys_pgtable[i]);
    }

    for(i = 0; i < MAX_SWAP_PAGES; i++){
      reset_page_data(&proc->pageTable.swp_pgtable[i]);
    }
  }
}

void 
pageFaultHandler(struct proc* curproc, uint va_fault){
	// if needed, check that myproc() hasn't changed to init or shell:
	uint va_fault_base = PGROUNDDOWN(va_fault);
	int page_index = -1;
  int i;
	for(i = 0; i < MAX_SWAP_PAGES; i++){
		if(curproc->pageTable.swp_pgtable[i].virt_addr == va_fault_base){
			page_index = i;
			break;
		}
	}

  // if page_index is -1, it means that the va_page_base is not in the swap file
  // in this case kill the process and throw a panic // AVI 13/5 
	if(page_index < 0){
    curproc->killed = 1;
    cprintf("pid %d %s: Segmentation fault\n", curproc->pid, curproc->name);
    panic("Segmentation fault!");
		return;
	}

  // create a temp struct of pageData for copy purposes, and populate it
	struct page_data pageData;
	copy_page_data(&pageData, &curproc->pageTable.swp_pgtable[page_index]);

	// get the page table entry, and since it's swapped out, we don't create another.
  // we are sure 100% that its swapped out since we made it past the page_index check
	pte_t* pte = walkpgdir(curproc->pgdir, (void*) pageData.virt_addr, 0);

  // if pte is 0, it means that the virtual address falls under an unallocated table for the process
  // its basically like the check of page_table, but for consistency we check this.
  // in this case kill the process and throw a panic // AVI 13/5 
  if(pte == 0){
    curproc->killed = 1;
    cprintf("pid %d %s: Segmentation fault2\n", curproc->pid, curproc->name);
    panic("Segmentation fault!2");
    return;
  }

	char* page_mem = kalloc(); // allocate a page.
	if(page_mem == 0){
		panic("No more memory!");
	}

	// read page from swap file:
	readFromSwapFile(curproc, page_mem, pageData.file_offset*PGSIZE, PGSIZE);
	mappages(curproc->pgdir, (char*)pageData.virt_addr, PGSIZE, V2P(page_mem), PTE_W|PTE_U);
	reset_page_data(&curproc->pageTable.swp_pgtable[page_index]);
  curproc->pageTable.swp_pagecount--;
  if(curproc->pageTable.phys_pagecount == MAX_PSYC_PAGES - 1)
    swap_pages(curproc);
	// Set page status:
	pageData.in_physmem = 1;
	pageData.in_swp = 0;
	// make sure to increase and decrease phys_pagecount
	copy_page_data(&curproc->pageTable.phys_pgtable[curproc->pageTable.phys_pagecount],&pageData); // AVI 13/5 

  // inc the count of the sageouts
  curproc->pageTable.pgout_count++;
	// set the page table entry's flag to not swapped:
	*pte = *pte & ~PTE_PG;
}

// AVI 18/5
void pages_reorder(int chosen){
  int len = myproc()->pageTable.phys_pagecount;
  struct page_data temp[len];

  int i = 0;
  int j;
  for(j = (chosen + 1) % len ; i < len ; j = (j + 1) % len){
    copy_page_data(&temp[i], &myproc()->pageTable.phys_pgtable[j]);
    i++;
  }

  for(j = 0 ; j < len ; j++){
    copy_page_data(&myproc()->pageTable.phys_pgtable[j], &temp[j]);
  }
}

// TODO: Implement page swapping using the different policies // AVI 18/5
void
swap_pages(struct proc* curproc){

  // tghe chosen page number
  int chosen = -1;

  #ifdef NFUA
  chosen = chose_nfua(curproc);
  #endif

  #ifdef LAPA
  chosen = chose_lapa(curproc);
  #endif

  #ifdef SCFIFO
  chosen = chose_scfifo(curproc);
  #endif

  #ifdef AQ
  chosen = chose_aq(curproc);
  #endif


  if(chosen == -1){
    panic("did not find a page to swap");
  }

  uint va = curproc->pageTable.phys_pgtable[chosen].virt_addr;
  pte_t* pte = walkpgdir(curproc->pgdir,(char*)PGROUNDDOWN(va),0);
  
  int freePage = -1;
  int i;
  for(i = 0 ; i < MAX_SWAP_PAGES ; i++) {
    if (curproc->pageTable.swp_pgtable[i].in_swp == 0) {
      freePage = i;
      break;
    }
  }

  if(freePage == -1){
        panic("No room in file");
  }
  
  cprintf("[swap_pages] Process no.%d swapped page with virtual address: %d\n",curproc->pid,va);
  
  pages_reorder(chosen);

  struct page_data* swappedPage = &curproc->pageTable.swp_pgtable[freePage];
  curproc->pageTable.swp_pagecount++;
  curproc->pageTable.phys_pagecount--;
  curproc->pageTable.pgout_count++;
  swappedPage->virt_addr = va;
  swappedPage->page_tbl_entry = pte;
  swappedPage->in_swp = 1;
  swappedPage->in_physmem = 0;
  swappedPage->file_offset = freePage;
  #if defined (NFUA) || defined (SCFIFO) || defined (AQ)
  swappedPage->access_count = 0;
  #endif

  #ifdef LAPA
  swappedPage->access_count = 0xFFFFFFFF;
  #endif
  swappedPage->creation_time = ticks;

  writeToSwapFile(curproc,(char*)va,freePage*PGSIZE, PGSIZE);

  char* physical_address_to_free = (char*)P2V(PTE_ADDR(*pte));
  *pte &= ~PTE_P;   //clear present bit
  *pte |= PTE_PG;   //set paged out bit
  *pte &= ~PTE_A;   //clear Access bit
  kfree(physical_address_to_free); // free psyc memory (page)
  lcr3(V2P(curproc->pgdir)); // TLB REFRESH
}


// AVI 17/5
void
access_count_update_for_single_process(struct proc* p) {
  int i = 0;
  for(i = 0 ; i < p->pageTable.phys_pagecount ; i++){
    if(p->pageTable.phys_pgtable[i].virt_addr != 0){
      pte_t* pte = walkpgdir(p->pgdir,(char*)PGROUNDDOWN(p->pageTable.phys_pgtable[i].virt_addr), 0);
      p->pageTable.phys_pgtable[i].access_count = p->pageTable.phys_pgtable[i].access_count >> 1;
      if(*pte & PTE_A){
        p->pageTable.phys_pgtable[i].access_count |= (1 << 31);
      }
    }
  }
}

// AVI 20/5
void
advance_queue_for_single_process(struct proc* p) {
  int i = 0;
  // cprintf("proc p pagecount: %d\n", p->pageTable.phys_pagecount);
  for(i = 0 ; i < p->pageTable.phys_pagecount ; i++){
    if(p->pageTable.phys_pgtable[i].virt_addr != 0){
      pte_t* pte1 = walkpgdir(p->pgdir,(char*)PGROUNDDOWN(p->pageTable.phys_pgtable[i].virt_addr), 0);
      pte_t* pte2 = walkpgdir(p->pgdir,(char*)PGROUNDDOWN(p->pageTable.phys_pgtable[i+1].virt_addr), 0);
      if(*pte1 & PTE_A){
        *pte1 &= ~PTE_A;
        if(!(*pte2 & PTE_A)) {
          struct page_data temp;
          copy_page_data(&temp, &p->pageTable.phys_pgtable[i]);
          copy_page_data(&p->pageTable.phys_pgtable[i], &p->pageTable.phys_pgtable[i+1]);
          copy_page_data(&p->pageTable.phys_pgtable[i+1], &temp);
          // cprintf("hi!===============================================\n");
        }
      }
    }
  }
}


// AVI 18/5
void
add_phys_page(uint va ,uint* pte) {  
  cprintf("[add_phys_page] Proccess no.%d added physical page with virtual address %d\n", myproc()->pid, va);
  struct page_data* phys_page = &myproc()->pageTable.phys_pgtable[myproc()->pageTable.phys_pagecount];
  phys_page->virt_addr = va;
  phys_page->page_tbl_entry = pte;

  phys_page->in_swp = 0;
  phys_page->in_physmem = 1;
  
  phys_page->file_offset = -1;
  #if defined (NFUA) || defined (SCFIFO) || defined (AQ)
  phys_page->access_count = 0;
  #endif

  #ifdef LAPA
  phys_page->access_count = 0xFFFFFFFF;
  #endif
  
  phys_page->creation_time = ticks;
  myproc()->pageTable.phys_pagecount = myproc()->pageTable.phys_pagecount + 1;
  *pte |= PTE_P;   //turn on present bit
  *pte &= ~PTE_PG;   //clears paged out bit
}

int
calc_ones_count(int acess_count) {
  int ac = acess_count;
  int ones_count = 0;
  while(ac > 0) {
    if(ac % 2 == 1) {
      ones_count++;
    }
    ac = ac >> 1;
  }
  return ones_count;
}

// AVI 18/5
int
chose_nfua(struct proc* p){
  cprintf("[chose_nfua] head of function\n");
  uint chosen = -1;
  uint minimal_access_count =  0x7FFFFFFF;
  // cprintf("minimal_access_count = %d\n", minimal_access_count);
  int i;
  for(i = 0; i < p->pageTable.phys_pagecount; i++) {
    if(p->pageTable.phys_pgtable[i].access_count <= minimal_access_count) {
      minimal_access_count = p->pageTable.phys_pgtable[i].access_count;
      chosen = i;
    }
  }
  return chosen;
}


// AVI 18/5
int
chose_lapa(struct proc* p){
  cprintf("[chose_lapa] head of function\n");
  uint chosen = -1;
  uint minimal_ones_count = 32;
  // cprintf("minimal_access_count = %d\n", minimal_access_count);
  int i;
  for(i = 0; i < p->pageTable.phys_pagecount; i++) {
    int curr_ones_count = calc_ones_count(p->pageTable.phys_pgtable[i].access_count);
    if(curr_ones_count <= minimal_ones_count) {
      minimal_ones_count = curr_ones_count;
      chosen = i;
    }
  }
  return chosen;
}


// AVI 18/5
int
chose_scfifo(struct proc* p){
  cprintf("[chose_scfifo] head of function\n");
  int chosen = -1;
  pte_t* pte;
  int i;
  for(i = 0 ; chosen == -1 ; i = (i + 1) % p->pageTable.phys_pagecount){
    pte = walkpgdir(p->pgdir,(char*)PGROUNDDOWN(p->pageTable.phys_pgtable[i].virt_addr), 0);
    if(*pte & PTE_A){
      *pte = *pte & ~PTE_A;
      //creation time is the time we insert the page to the physical memory
      p->pageTable.phys_pgtable[i].creation_time = ticks; // maybe should be aded 1
    }else{
      chosen = i;
    }
  }
  return chosen;
}

// AVI 18/5
int
chose_aq(struct proc* p){
  cprintf("[chose_aq] head of function\n");
  return 0;
}