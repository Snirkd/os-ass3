#include "types.h"
#include "stat.h"
#include "user.h"

#define PGSIZE 4096

int
main(int argc, char *argv[])
{
	uint iterations = 20; // maybe change to 20
    void* pages[iterations];
    int forked1, forked2;
    
    forked1 = fork();
    
    int i;
    for (i = 0; i < iterations; i++) {
        pages[i]=sbrk(PGSIZE);
    }

    processPagingDataPrint();

    for (i = 0; i < iterations; i++) {
        *(int *)pages[i] = i;
    }
    
    forked2=fork();
    
    for (i = 0; i < iterations; i++) {
       printf(1,"[myMemTest] proc %d, pid: %d, va = %d\n", i%2+3, getpid(), *((int *)pages[i%2+3]));
       printf(1,"[myMemTest] proc %d, pid: %d, va = %d\n", i, getpid(), *((int *)pages[i]));
    }

    if (forked1 > 0)
        wait();
    
    if (forked2 > 0)
        wait();


   printf(1,"***process no.%d finished executing***\n", getpid());
   
   exit();
}